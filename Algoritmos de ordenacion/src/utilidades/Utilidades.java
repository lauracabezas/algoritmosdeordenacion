/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.util.Objects;

/**
 *
 * @author Nando Ortiz
 */
public class Utilidades {
    public static boolean compararArrays(Integer[] array1, Integer[] array2)
    {
        boolean iguales = array1.length == array2.length;
        for(int i = 0; i < array1.length && iguales; i++)
        {
            iguales = Objects.equals(array1[i], array2[i]);
        }

        return iguales;
    }
}
