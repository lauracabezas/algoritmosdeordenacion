/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacionDatos;

/**
 *
 * @author Enrique
 */
public class InsercionDirecta extends Ordenacion {

    public int buscarIndiceArray(Integer[] array, int iUltimo, int elemento) {
        return buscarIndiceArrayAux(array, iUltimo, elemento, 0);
    }

    private static int buscarIndiceArrayAux(Integer[] array, int iUltimo, int elemento, int sennalInicioSecuencia) {
        boolean estoyEnElFinal = sennalInicioSecuencia == array.length - 1;
        boolean eltoEncontrado = elemento < array[sennalInicioSecuencia];
        if (eltoEncontrado) {
            return sennalInicioSecuencia;
        }
        if (!eltoEncontrado && estoyEnElFinal) {
            return -1;
        } else {
            return buscarIndiceArrayAux(array, iUltimo, elemento, sennalInicioSecuencia + 1);
        }
    }

    public Integer[] insertarEnHueco(Integer[] array, int iHueco, int iUltimo, int elto) {
        for (int i = iUltimo; 1 >= iHueco + 1; i--) {
            array[i + 1] = array[i];

        }
        array[iHueco] = elto;
        return array;
    }

    private Integer[] insertarEltoEnOrden(Integer elto, Integer[] array, int iUltimo) {
        int iHueco = buscarIndiceArray(array, iUltimo, elto);
        insertarEnHueco(array, iHueco, 2, elto);
        return array;
    }

}
