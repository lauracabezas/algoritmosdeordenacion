package ordenacionDatos;

import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;

import org.junit.Test;

public class OrdenacionTest{

    protected Ordenacion algoritmo;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testOrdenarListaVacia()
    {
        Integer[] arrayInicial = {};
        Integer[] arrayEsperado = {};
        Integer[]  arrayObtenido = algoritmo.ordenar(arrayInicial);
        assertTrue(compararArrays(arrayEsperado, arrayObtenido)); 
    }

    @Test
    public void testOrdenarOrdenada()
    {
        Integer[] arrayInicial = {1, 3, 5, 7};
        Integer[] arrayEsperado = {1, 3, 5, 7};
        Integer[]  arrayObtenido = algoritmo.ordenar(arrayInicial);
        assertTrue(compararArrays(arrayEsperado, arrayObtenido)); 
    }

    @Test
    public void testOrdenarListaInvertida()
    {
        Integer[] arrayInicial = {7, 3, 5, 1};
        Integer[] arrayEsperado = {1, 3, 5, 7};
        Integer[]  arrayObtenido = algoritmo.ordenar(arrayInicial);
        assertTrue(compararArrays(arrayEsperado, arrayObtenido)); 
    }

    @Test
    public void testOrdenarListaConRepetidos()
    {
        Integer[] arrayInicial = {7, 5, 7, 1, 3, 1, 5, 3, 7};
        Integer[] arrayEsperado = {1, 1, 3, 3, 5, 5, 7, 7, 7};
        Integer[]  arrayObtenido = algoritmo.ordenar(arrayInicial);
        assertTrue(compararArrays(arrayEsperado, arrayObtenido)); 
    }

    private boolean compararArrays(Integer[] array1, Integer[] array2)
    {
        boolean iguales = array1.length == array2.length;
        for(int i = 0; i < array1.length && iguales; i++)
        {
            iguales = Objects.equals(array1[i], array2[i]);
        }

        return iguales;
    }
}