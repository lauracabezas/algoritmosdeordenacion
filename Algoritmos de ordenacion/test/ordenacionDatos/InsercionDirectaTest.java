/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacionDatos;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utilidades.Utilidades;


/**
 *
 * @author Nando Ortiz
 */
public class InsercionDirectaTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    
    //TESTS PARA PROBAR buscarIndiceArray
    @Test
    public void testBuscarHuecoSubarrayUnElto() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {4, 3, 8, 5, 1, 7};
        int resultadoEsperado = 0;
        int resultadoObtenido = insercionDirecta.buscarIndiceArray(array, 0, 3);
        assertEquals(resultadoEsperado,resultadoObtenido);
    }

    @Test
    public void testBuscarHuecoSubarraySinMoverElementos() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {3, 4, 8, 5, 1, 7};
        int resultadoEsperado = 2;
        int resultadoObtenido = insercionDirecta.buscarIndiceArray(array, 8, 7);
        assertEquals(resultadoEsperado,resultadoObtenido);
    }

    @Test
    public void testBuscarHuecoSubarrayConUnSoloIntercambio() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {3, 4, 8, 5, 1, 7};
        int resultadoEsperado = 2;
        int resultadoObtenido = insercionDirecta.buscarIndiceArray(array, 2, 5);
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    
    @Test
    public void testBuscarHuecoSubarrayConVariosIntercambiosLlevantoElEltoAlPrincipio() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {3, 4, 5, 8, 1, 7};
        int resultadoEsperado = 0;
        int resultadoObtenido = insercionDirecta.buscarIndiceArray(array, 3, 1);
        assertEquals(resultadoEsperado,resultadoObtenido);
    }

    @Test
    public void testBuscarHuecoSubarrayDondeElEltoEsElUltimoDelArrayCompleto() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {1, 3, 4, 5, 8, 7};
        int resultadoEsperado = 4;
        int resultadoObtenido = insercionDirecta.buscarIndiceArray(array, 4, 7);
        assertEquals(resultadoEsperado,resultadoObtenido);
    }

    //TESTS PARA PROBAR buscarIndiceArray
    @Test
    public void testInsertarEnHuecoUnElto() {
        InsercionDirecta insercionDirecta = new InsercionDirecta();
        Integer[] array = {4, 3, 8, 5, 1, 7};
        Integer[] arrayEsperado = {3, 4, 8, 5, 1, 7};
        insercionDirecta.insertarEnHueco(array, 3, 0, 0);
        assertTrue(Utilidades.compararArrays(arrayEsperado, array));
    }
}
 