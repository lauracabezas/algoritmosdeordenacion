/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacionDatos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Enrique
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ordenacionDatos.OrdenacionTest.class, ordenacionDatos.IntercambioDirectoTest.class, ordenacionDatos.MergesortTest.class, ordenacionDatos.SeleccionDirectaTest.class, ordenacionDatos.InsercionDirectaTest.class, ordenacionDatos.QuicksortTest.class})
public class OrdenacionDatosSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
