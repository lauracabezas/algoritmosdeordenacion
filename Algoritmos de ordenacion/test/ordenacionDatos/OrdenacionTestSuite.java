/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenacionDatos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Esta clase es un test suite para disparar todos los tests de la lógica de negocio.
 * @author mariano
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({InsercionDirectaTest.class, SeleccionDirectaTest.class, IntercambioDirectoTest.class, MergesortTest.class, QuicksortTest.class})
public class OrdenacionTestSuite {}
